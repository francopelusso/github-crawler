# Github Crawler
This repository contains **Python's classes** to scrap content from the first page of a Github's search result website. However, they are easily extensible to make them scrap a different content.
Furthermore, it also brings a **command-line script** to scrap github with a single line in your console.


## Install
To run this project in your own machine, you should follow these steps:

1. Install **Python** (*3.7 recommended*) and **python-virtualenv**. For example: `sudo apt-get install python3.7 python-virtualenv` or `sudo pacman -S python3 python-virtualenv`.
1. Make your own virtual environment: `python -m venv ~/Envs/gh_crawler`.
1. Activate virtual environment: `source ~/Envs/gh_crawler/bin/activate`.
1. Clone this repository: `git clone git@gitlab.com:francopelusso/github_crawler.git`.
1. Change your current working directory: `cd github_crawler`.
1. Install dependencies into your environment: `pip install -r requirements.txt`.
1. You're ready to go! You can now import classes from **crawlers.py** into your own code or   run the command-line script **crawl_github.py** `python crawl_github.py test_files/simple.json`.


```bash
    user@pc:~ $ sudo apt-get install python3.7 python-virtualenv
    user@pc:~ $ python3.7 -m venv ~/Envs/gh_crawler
    user@pc:~ $ source ~/Envs/gh_crawler/bin/activate
    (gh_crawler) user@pc:~ $ git clone git@gitlab.com:francopelusso/github_crawler.git
    (gh_crawler) user@pc:~ $ cd github_crawler
    (gh_crawler) user@pc:~ $ pip install -r requirements.txt
    (gh_crawler) user@pc:~ $ python crawl_github.py test_files/simple.json
    (gh_crawler) user@pc:~ $ chmod +x crawl_github.py
    (gh_crawler) user@pc:~ $ ./crawl_github.py test_files/simple.json
```


## Usage
Both classes and script take a **JSON string** or a **.json filename** as *input*, process those arguments and *output* a **JSON string** indented by two spaces: *classes return it while the script prints it in stdout*.

### Example Input
```json
{
  "keywords": [
    "django",
    "django-rest-framework",
    "python"
  ],
  "proxies": [
    "23.236.180.234:80",
    "95.158.62.112:44226"
  ],
  "type": "Repositories"
}
```
1. **keywords**: Words to search in github.
1. **proxies**: Proxies to use for requests.
1. **type**: Type of objects to search for. Supported types are "*Repositories*", "*Wikis*" and "*Issues*".

### Example Output
```json
[
  {
    "url": "https://github.com/cozzin/hana-qt-server/wiki/Home"
  },
  {
    "url": "https://github.com/cusey/DjangoRestExamples/wiki/Home"
  },
  {
    "url": "https://github.com/agh-glk/fcs/wiki/Technologies"
  },
  {
    "url": "https://github.com/JeongtaekLim/TIL/wiki/JWT,-Superuser"
  },
  {
    "url": "https://github.com/B-X-G/bxgcloud/wiki/%E9%A1%B9%E7%9B%AE%E6%9E%84%E9%80%A0"
  },
  {
    "url": "https://github.com/aaronoppenheimer/mando/wiki/setup"
  },
  {
    "url": "https://github.com/karthikrk1/Django-profiles/wiki/Home"
  },
  {
    "url": "https://github.com/ninemilli-song/all-season-investor/wiki/%E5%AD%A6%E4%B9%A0%E8%B7%AF%E5%BE%84"
  },
  {
    "url": "https://github.com/IntegratedAlarmSystem-Group/ias-webserver/wiki/Framework-Comparison-Django-vs-Pyramid"
  },
  {
    "url": "https://github.com/Wanderson304/Python/wiki/6.-Resetful-API-e-Micro-Servi%C3%A7os"
  }
]
```

It is an *array* containing *objects* which map the key "*url*" to each result's url.


## Extra data for repositories
When searching **repositories** you can get, in addition, two extra fields: *owner's name* and *language stats*.
However, this implies making an HTTP request for each repository which is a bit **slower**, so it is **disabled by default**. To **enable** it, you must *set the environment variable* **EXTRA_REPO** to the string '**true**' (*case-insensitive*). This can be accomplished by typing the following in your console: `export EXTRA_REPO=true`.
On the other hand, the environment variable can also be *set from a python file* by running the following: `import os; os.environ['EXTRA_REPO'] = 'true'` before running the classes' **generate_output** method.

### Example input
```json
{
  "proxies": [
    "23.236.180.234:80",
    "95.158.62.112:44226"
  ],
  "keywords": [
    "django",
    "django-rest-framework",
    "python"
  ],
  "type": "Repositories"
}
```

### Example output with extra data
```json
[
  {
    "url": "https://github.com/RealmTeam/django-rest-framework-social-oauth2",
    "extra": {
      "owner": "RealmTeam",
      "language_stats": {
        "Python": 100.0
      }
    }
  },
  {
    "url": "https://github.com/codingforentrepreneurs/REST-API",
    "extra": {
      "owner": "codingforentrepreneurs",
      "language_stats": {
        "Python": 100.0
      }
    }
  },
  {
    "url": "https://github.com/nomadcoders/nomadgram",
    "extra": {
      "owner": "nomadcoders",
      "language_stats": {
        "JavaScript": 41.9,
        "Python": 40.5,
        "HTML": 10.8,
        "CSS": 6.8
      }
    }
  },
  {
    "url": "https://github.com/twtrubiks/DRF-dataTable-Example-server-side",
    "extra": {
      "owner": "twtrubiks",
      "language_stats": {
        "HTML": 58.3,
        "CSS": 19.7,
        "Python": 16.3,
        "JavaScript": 5.7
      }
    }
  },
  {
    "url": "https://github.com/JuanBenitez97/django-rest-framework-crud",
    "extra": {
      "owner": "JuanBenitez97",
      "language_stats": {
        "Python": 100.0
      }
    }
  },
  {
    "url": "https://github.com/LondonAppDeveloper/byob-profiles-rest-api",
    "extra": {
      "owner": "LondonAppDeveloper",
      "language_stats": {
        "Python": 92.7,
        "Shell": 7.3
      }
    }
  },
  {
    "url": "https://github.com/felix-d/Django-Oauth-Toolkit-Python-Social-Auth-Integration",
    "extra": {
      "owner": "felix-d",
      "language_stats": {
        "Python": 100.0
      }
    }
  },
  {
    "url": "https://github.com/hugobrilhante/drf-tutorial-pybr11",
    "extra": {
      "owner": "hugobrilhante",
      "language_stats": {
        "Python": 100.0
      }
    }
  },
  {
    "url": "https://github.com/pointworld/mxshop",
    "extra": {
      "owner": "pointworld",
      "language_stats": {
        "Python": 99.0,
        "HTML": 1.0
      }
    }
  },
  {
    "url": "https://github.com/LondonAppDev/byob-profiles-rest-api",
    "extra": {
      "owner": "LondonAppDev",
      "language_stats": {
        "Python": 100.0
      }
    }
  }
]
```


## Continuous Integration (CI)
This repository uses gitlab's CI tools to run tests automatically on every push.
It runs in two stages: **check** and **test**.

### Check
1. **Codestyle Check**: It makes sure the code fulfills PEP8 standards.
1. **CrawlGithub Check**: It makes sure the crawl_github.py file can be run.

### Test
1. **Test**: It runs pytest to make sure the projects pass every test in **test_crawl_github.py** and **test_crawlers.py** files.
