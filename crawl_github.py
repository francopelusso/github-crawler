#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from crawlers import GithubCrawler


def main():
    if len(sys.argv) < 2:
        print('You must provide JSON-like input or JSON filename.')
        return

    g = GithubCrawler(sys.argv[1:])
    output = g.crawl()
    if output:
        print(output)


def init():
    if __name__ == '__main__':
        main()


init()
