import crawl_github
import os
import pytest
import requests
from crawlers import BaseCrawler, GithubCrawler


REPO_LIST = [
    'https://github.com/xiuyanduan/xiuyanduan.github.io',
    'https://github.com/lambdamai/lambda-help'
]


def get_crawler_from_filename(filename, crawler_class=BaseCrawler):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return crawler_class([f'{dir_path}/test_files/{filename}'])


@pytest.fixture
def data_fixture():
    parsed_dict = (
        '['
        '\n  {'
        '\n    "url": "https://github.com/xiuyanduan/xiuyanduan.github.io"'
        '\n  },'
        '\n  {'
        '\n    "url": "https://github.com/lambdamai/lambda-help"'
        '\n  }'
        '\n]'
    )
    return {'input': REPO_LIST, 'output': parsed_dict}


@pytest.fixture
def data_fixture_with_extra():
    parsed_dict = (
        '['
        '\n  {'
        '\n    "url": "https://github.com/xiuyanduan/xiuyanduan.github.io",'
        '\n    "extra": {'
        '\n      "owner": "xiuyanduan",'
        '\n      "language_stats": {'
        '\n        "HTML": 96.9,'
        '\n        "CSS": 2.1,'
        '\n        "JavaScript": 1.0'
        '\n      }'
        '\n    }'
        '\n  },'
        '\n  {'
        '\n    "url": "https://github.com/lambdamai/lambda-help",'
        '\n    "extra": {'
        '\n      "owner": "lambdamai",'
        '\n      "language_stats": {'
        '\n        "Jupyter Notebook": 91.1,'
        '\n        "Python": 8.1,'
        '\n        "CSS": 0.8'
        '\n      }'
        '\n    }'
        '\n  }'
        '\n]'
    )
    return {'input': REPO_LIST, 'output': parsed_dict}


class TestBaseCrawler():
    def test_initialize_and_parse(self):
        crawler = BaseCrawler((
            '{"keywords": ["django", "django-rest-framework", "python"],'
            '"proxies": ["23.236.180.234:80", "95.158.62.112:44226"],'
            '"type": "Wikis"}'
        ).split(' '))
        expected_args = {
            'proxies': ["23.236.180.234:80", "95.158.62.112:44226"],
            'keywords': ['django', 'django-rest-framework', 'python'],
            'type': 'Wikis'
        }
        assert crawler.args == expected_args

    def test_initialize_and_parse_filename(self):
        crawler = get_crawler_from_filename('simple.json')
        expected_args = {
            'proxies': ["23.236.180.234:80", "95.158.62.112:44226"],
            'keywords': ['django', 'django-rest-framework', 'python'],
            'type': 'Wikis'
        }
        assert crawler.args == expected_args

    def test_initialize_and_parse_bad_json(self):
        crawler = BaseCrawler((
            '{keywords": ["django", "django-rest-framework", "python"],'
            '"proxies": [23.236.180.234:80", "95.158.62.112:44226"],'
            '"type": "Wikis}'
        ).split(' '))
        assert crawler.args is None

    def test_initialize_and_parse_bad_json_filename(self):
        crawler = get_crawler_from_filename('bad_format.json')
        assert crawler.args is None

    def test_initialize_and_parse_filename_not_exists(self):
        crawler = get_crawler_from_filename('not_exists.json')
        assert crawler.args is None

    def test_request_webpage(self):
        crawler = BaseCrawler('{}')
        r = crawler.request_webpage('https://google.com')
        assert isinstance(r, requests.Response)

    def test_request_webpage_bad_url(self):
        crawler = BaseCrawler('{}')
        with pytest.raises(requests.exceptions.HTTPError):
            assert crawler.request_webpage(
                'https://github.com/nobodyNOTEXISTS/project'
            )

    def test_crawl(self):
        crawler = get_crawler_from_filename('url.json')
        assert crawler.crawl().startswith('"<!doctype html>')

    def test_crawl_bad_url(self, caplog):
        crawler = get_crawler_from_filename('bad_url.json')
        crawler.crawl()
        assert '404 Client Error: Not Found for url' in caplog.text

    def test_crawl_bad_args(self, caplog):
        crawler = BaseCrawler('{}')
        crawler.crawl()
        assert "Can't crawl without arguments." in caplog.text

    def test_crawl_bad_proxies(self, caplog):
        crawler = get_crawler_from_filename('bad_proxies.json')
        crawler.crawl()
        assert "Can't connect to proxy." in caplog.text


class TestGithubCrawler():
    def test_initialize_default_args(self):
        crawler = get_crawler_from_filename('basic.json', GithubCrawler)
        expected_args = {
            'method': 'get',
            'url': 'https://github.com/search/',
            'proxies': {},
            'params': {
                'q': 'django+django-rest-framework+python',
                'type': 'Repositories'
            }
        }
        assert crawler.args == expected_args

    def test_initialize_missing_args(self, caplog):
        crawler = GithubCrawler('{}')
        assert 'Input must have: ' in caplog.text

    def test_get_data(self):
        crawler = get_crawler_from_filename('simple.json', GithubCrawler)
        d = crawler.get_data(crawler.request_webpage(**crawler.args))
        assert len(d) == 10
        for url in d:
            assert 'https://github.com/' in url

    def test_generate_output(self, data_fixture):
        crawler = GithubCrawler('{}')
        output = crawler.generate_output(data_fixture['input'])
        assert output == data_fixture['output']

    def test_get_extra_data(self):
        crawler = GithubCrawler('{}')
        extra = crawler.get_extra_data(
            'https://github.com/encode/django-rest-framework'
        )
        expected_output = {
            "owner": "encode",
            "language_stats": {
                "Python": 90.5,
                "HTML": 5.7,
                "CSS": 2.6,
                "JavaScript": 1.2
            }
        }
        assert extra == expected_output

    def test_generate_output_with_extra(self, data_fixture_with_extra):
        crawler = get_crawler_from_filename('basic.json', GithubCrawler)
        os.environ['EXTRA_REPO'] = 'true'
        output = crawler.generate_output(data_fixture_with_extra['input'])
        assert output == data_fixture_with_extra['output']
