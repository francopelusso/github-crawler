# -*- coding: utf-8 -*-
import json
import logging
import os
import random
import requests
from bs4 import BeautifulSoup


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class BaseCrawler():
    """Simple web scrapper from which specific scrappers inherit.

    Attributes:
        DEFAULTS (dict): default values for missing inputs.
        REQUIRED_ARGS (tuple): Arguments which the input MUST have.
    """

    DEFAULTS = {}
    REQUIRED_ARGS = ()

    def __init__(self, args):
        try:
            logger.debug('Reading arguments.')
            self.args = self.parse_args(args)
            self.args = None if self.args == {} else self.args
            return
        except FileNotFoundError:
            logger.error(f"File {args[0]} doesn't exist.")
        except json.decoder.JSONDecodeError:
            logger.error(f"Can't read {' '.join(args)} as JSON.")
        self.args = None

    def parse_args(self, args):
        """Read arguments from json list or file input.

        Args:
            args (list): A ' ' split json-like string or a single
                element containing a .json filename.

        Returns:
            dict: Parsed input.
        """

        if args[0].endswith('.json'):
            logger.debug('Getting args from file.')
            return json.load(open(args[0], 'r'))
        else:
            logger.debug('Getting args from string.')
            return json.loads(' '.join(args))

    def request_webpage(self, url, method='get', proxies={}, params={}):
        """Send request to webpage and return the response object.

        Args:
            url (str): Website's url to request.
            method (str): HTTP Verb (defaults to 'get').
            proxies (dict): Contains proxies with their protocol and
                ips data (defaults to {})
            params (dict): Contains arguments for the request, params
                or date depending on method (defaults to {})

        Returns:
            requests.Response: HTTP Response object with obtained data.
        """

        params_name = 'data' if method == 'post' else 'params'
        if proxies:
            proxies = {proxies['protocol']: random.choice(proxies['proxies'])}
        kwargs = {params_name: params, 'proxies': proxies}
        logger.debug('Sending request to website.')
        r = requests.request(method.upper(), url, **kwargs)
        r.raise_for_status()
        return r

    def get_soup(self, html_content):
        return BeautifulSoup(html_content, features="html.parser")

    def generate_output(self, data):
        return json.dumps(data)

    def get_data(self, response):
        return response.text

    def crawl(self):
        """Make request to website and scrap content.

        Take the args defined on initialization, make the http request
        and generate JSON output.

        Returns:
            str: JSON representation of obtained response data.
        """

        try:
            r = self.request_webpage(**self.args)
        except requests.HTTPError as e:
            logger.error(e)
        except requests.exceptions.ProxyError:
            logger.error("Can't connect to proxy. Check if data is valid.")
        except TypeError as e:
            logger.error("Can't crawl without arguments.")
        else:
            d = self.get_data(r)
            logger.debug('Generating output.')
            return self.generate_output(d)


class GithubCrawler(BaseCrawler):
    """Github scrapper which reads a single page from a search result.

    Attributes:
        DEFAULTS (dict): default values for missing inputs.
        REQUIRED_ARGS (tuple): Arguments which the input MUST have.
        BASE_URL (str): Website root's url used to make absolute paths.
        LIST_ITEMS (dict): Maps type to list-item class name's start.
    """

    DEFAULTS = {
        'url': 'https://github.com/search/',
        'method': 'get',
        'type': 'Repositories'
    }
    REQUIRED_ARGS = ('keywords', )
    BASE_URL = 'https://github.com'
    LIST_ITEMS = {'Repositories': 'repo', 'Wikis': 'wiki', 'Issues': 'issue'}

    def parse_args(self, args):
        """Make dictionary from input json.

        Args:
            args (list): A ' ' split json-like string or a single
                element containing a .json filename.

        Returns:
            dict: Parsed input.
        """

        input_args = super().parse_args(args)
        if any([a not in input_args.keys() for a in self.REQUIRED_ARGS]):
            logger.error(f"Input must have: {', '.join(self.REQUIRED_ARGS)}.")
            return

        proxies = input_args.get('proxies')
        if proxies:
            proxies = {'protocol': 'https', 'proxies': proxies}
        else:
            proxies = {}

        return {
            'url': input_args.get('url', self.DEFAULTS['url']),
            'method': input_args.get('method', self.DEFAULTS['method']),
            'proxies': proxies,
            'params': {
                'q': '+'.join(input_args['keywords']),
                'type': input_args.get('type', self.DEFAULTS['type'])
            }
        }

    def get_data(self, response):
        """Extracts the urls from the response content.

        Finds list items by class and extracts urls having
        data-hydro-click attribute, which only the resulting wiki
        entry, repository or issue have.

        Args:
            response (requests.Response): Github's search results.

        Returns:
            list: Contains all the urls ready to display.
        """

        soup = self.get_soup(response.text)
        urls = []
        logger.debug('Iterating through list items.')
        li_class_name = (
            f"{self.LIST_ITEMS[self.args['params']['type']]}-list-item"
        )
        for li in soup.find_all(class_=li_class_name):
            item_a = li.find(**{'data-hydro-click': True})
            urls.append(f"{self.BASE_URL}{item_a.attrs['href']}")
        return urls

    def generate_output(self, data):
        """Convert url list to JSON dict output.

        Requesting every repository to get language stats is slow so it
        is disabled by default. You must set the environment variable
        EXTRA_REPO to 'true' to use it (e.g.: export EXTRA_REPO=true).

        Args:
            data (list): Contains every url from the search result.

        Returns:
            str: JSON-like string output.
        """
        extra_repo_data = os.getenv('EXTRA_REPO', 'false').lower() == 'true'
        params = self.args['params'] if self.args else {'type': ''}
        if params['type'] == 'Repositories' and extra_repo_data:
            output = [
                {"url": url, "extra": self.get_extra_data(url)}
                for url in data
            ]
            return json.dumps(output, indent=2)

        return json.dumps([{"url": url} for url in data], indent=2)

    def get_extra_data(self, url):
        return {
            "owner": url.split('/')[3],
            "language_stats": self.get_language_stats(url)
        }

    def get_language_stats(self, url):
        """Get language usage data.

        Args:
            url (str): Repository url to request.

        Returns:
            dict: It maps language as key to percentage as value.
        """
        proxies = self.args['proxies'] if self.args else {}
        r = self.request_webpage(url, proxies=proxies)
        soup = self.get_soup(r.text)
        lookup_dict = {'class_': 'language-color', 'aria-label': True}
        languages = {}
        for language_item in soup.find_all(**lookup_dict):
            language_data = language_item.attrs['aria-label'].split(' ')
            language = ' '.join(language_data[0:-1])
            percent = language_data[-1]
            languages[language] = float(percent.replace('%', ''))
        return languages
