import crawl_github
import sys
from unittest.mock import patch


class TestCrawlGithubScript():
    def test_main(self, capsys):
        args = ['./crawl_github.py', './test_files/simple.json']
        with patch.object(sys, 'argv', args):
            crawl_github.main()
            out = capsys.readouterr().out
            assert '"url": "https://github.com/' in out

    def test_bad_input(self, capsys):
        args = ['./crawl_github.py']
        with patch.object(sys, 'argv', args):
            crawl_github.main()
            out = capsys.readouterr().out
            assert 'You must provide JSON-like input or JSON filename.' in out

    def test_init(self, capsys):
        args = ['./crawl_github.py']
        with patch.object(crawl_github, '__name__', '__main__'), \
                patch.object(sys, 'argv', args):
            crawl_github.init()
            out = capsys.readouterr().out
            assert 'You must provide JSON-like input or JSON filename.' in out

    def test_init_not_main(self, capsys):
        args = ['./crawl_github.py']
        with patch.object(sys, 'argv', 'args'):
            crawl_github.init()
            out = capsys.readouterr().out
            assert 'You must provide JSON-like input' not in out
